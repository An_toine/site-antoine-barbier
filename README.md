# site-antoine

![site web antoinebarbier - presentation](images-readme/IMG_9571.PNG)

![site web antoinebarbier - image 2](images-readme/IMG_9572.PNG)

![site web antoinebarbier - image 3](images-readme/IMG_9573.PNG)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
